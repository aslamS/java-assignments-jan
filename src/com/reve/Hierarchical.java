package com.reve;
//childclass 1
public class Hierarchical extends ParentClass {

	private void offName() {
		System.out.println("Global Logic");

	}
	private void offLocat() {
		System.out.println("bangalore");
	}
	public static void main(String[] args) {
		Hierarchical h=new Hierarchical();
		h.offLocat();
		h.offName();
		h.ownerDetails();
		h.ownerLocation();
		h.ownerVehilce();
	}
}
