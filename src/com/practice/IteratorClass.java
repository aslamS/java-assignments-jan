package com.practice;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorClass {
public static void main(String[] args) {
	ArrayList<String> a=new ArrayList<String>();
	a.add("a");
	a.add("b");
	a.add("c");
	a.add("d");
	a.add("e");
	
	System.out.println(a);
	Iterator<String> itr = a.iterator();
	while (itr.hasNext()) {
	Object next = itr.next();
	System.out.println(next);
		
	}
}
}
