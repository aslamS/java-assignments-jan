package com.practice;

public class strClass2 {
	public static void main(String[] args) {
		String s1 = ("welcome");//string constant pool area
		char[] c = { 's', 'e', 'l', 'e', 'n', 'i', 'u', 'm' };
		String s2 = ("welcome");//string constant pool area
		String s3 = new String("welcome");//stored in heap memmry
		String s4 = new String("Hello World");//stored in heap memmry
		String s5 = new String(c);//stored in heap memmry
		System.out.println(s5);
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		s1.concat("java");
		System.out.println(s1);
		s1 = s1.concat("java");
		System.out.println(s1);

	}
}
