package com.practice;

public class StringMethods {
	public static void main(String[] args) {
		String s = ("welcome to java");
		System.out.println("given string is :" + s);
		// length
		System.out.println("lenght is :" + s.length());
		// index
		System.out.println("index is :" + (s.length() - 1));
		// charAt()
		char c = s.charAt(5);
		System.out.println("char @5th index :" + c);

		// to uppercase
		System.out.println("upper case is :" + s.toUpperCase());
		// is empty
		if (s.isEmpty()) {
			System.out.println("String is empty");
		} else {
			System.out.println("String is not empty");
		}
		// start with
		if (s.startsWith("w") | s.startsWith("W")) {
			System.out.println("yes start with given content");

		} else {
			System.out.println("No it is not start with given content");

		}
		// end with
		boolean b = s.endsWith("a");
		System.out.println("Ends with :" + b);
	}
}
