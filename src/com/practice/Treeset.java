package com.practice;

import java.util.TreeSet;

public class Treeset {
public static void main(String[] args) {
	TreeSet<Integer> t=new TreeSet<Integer>();
	t.add(10);
	t.add(20);
	t.add(15);
	t.add(16);
	t.add(17);
	t.add(19);
	System.out.println(t);
	boolean b = t.contains(16);
	System.out.println(b);
	boolean c = t.remove(17);
	System.out.println(c);
	System.out.println(t);
}
}
