package com.practice;

public class Strclass {
	public static void main(String[] args) {
		String str="welcome";
		String str1="welcome";
		String str3=new String ("welcome");
		String str4="java";
		boolean a = str.equals(str1);//true
		boolean b = str.equals(str3);//true
		boolean c = str.equals(str4);//false
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		//equals() checks the contents and not the reference
		
		//q1 palindrome 
		//occurence of letter in a particular word
	boolean a1=	str==str1;//true
	boolean a2=	str==str3;//false
	boolean a3=	str==str4;//false
	boolean a4=	str1==str3;//false
	System.out.println(a1);
	System.out.println(a2);
	System.out.println(a3);
	System.out.println(a4);
		
	}

}
